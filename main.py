import logging
import configparser
import re
from datetime import datetime
from pyrogram import Client, Filters, InputPhoneContact
from aiogram import Bot, Dispatcher, executor, types

config = configparser.ConfigParser()
config.read("config.ini")

# Configure logging
logging.basicConfig(level=logging.INFO)

# Initialize bot and dispatcher
bot = Bot(token=config['credentials']['telegram-api'])
dp = Dispatcher(bot)
def message_to_str(message):
    strToSend = f'{datetime.now()} '
    if message["from"]["id"] == int(config["settings"]['owner_id']):
        strToSend+=f'from owner '
    elif "username" in message["from"]:
        strToSend+=f'from @{message["from"]["username"]} '
    elif 'first_name' in message["from"]:
        strToSend+=f'from {message["from"]["first_name"]} ({message["from"]["id"]}) '
    else:
        strToSend+=f'from {message["from"]["id"]} '
    if 'forward_from' in message:
        strToSend+='forwarded from '
        if "username" in message["forward_from"]:
            strToSend+=f'@{message["forward_from"]["username"]} '
        elif 'first_name' in message["forward_from"]:
            strToSend+=f'{message["forward_from"]["first_name"]} ({message["forward_from"]["id"]}) '
        else:
            strToSend+=f'{message["forward_from"]["id"]} '
    elif 'forward_sender_name' in message:
        strToSend+=f'forwerded from {message["forward_sender_name"]} '
    strToSend+=f'with "{message["text"]}"'
    return strToSend

def user_to_str(user):
    if user:
        strToSend = 'get user: '
        if user.username:
            strToSend+=f'@{user["username"]}'
        elif user.first_name:
            strToSend+=f'{user["first_name"]} ({user["id"]})'
        else:
            strToSend+=f'{user["id"]}'
        return strToSend
    else:
        return ' ';


def user_info_from_message(message):
    if message and 'from' in message:
        strToSend = 'This user info\n'
        if 'first_name' in message['from']:
            strToSend+=f'First name: {message["from"]["first_name"]}\n'
        if 'last_name' in message['from']:
            strToSend+=f'Last name: {message["from"]["last_name"]}\n'
        if 'username' in message['from']:
            strToSend+=f'Username: @{message["from"]["username"]}\n'
        if 'id' in message['from']:
            strToSend+=f'id: <code>{message["from"]["id"]}</code>\n'
        if message['chat']['id']<0:
            strToSend+=f'chat id:<code>{message["chat"]["id"]}</code>\n'
        return strToSend
    else:
        return ' ';

def user_info_from_user(user):
    #print(user)
    if user:
        strToSend = 'This user info\n'
        if user.first_name:
            strToSend+=f'First name: {user["first_name"]}\n'
        if user.last_name:
            strToSend+=f'Last name: {user["last_name"]}\n'
        if user.username:
            strToSend+=f'Username: @{user["username"]}\n'
        if user.id:
            strToSend+=f'id: <code>{user["id"]}</code>\n'
        return strToSend
    else:
        return ' ';



@dp.message_handler(commands=['start'])
async def send_welcome(message: types.Message):
    """
    This handler will be called when user sends `/start` or  command
    """
    with open("log.txt","a") as f:
        f.write(message_to_str(message)+'\n')
    await message.answer("Hello, i will help you with geting user's info by id, telephone, telegram username and etc., type <b>/help</b> for more info",parse_mode='html')

@dp.message_handler(commands=['help'])
async def send(message: types.Message):
    strToSend=f"<b>/get_me</b> - get my info\n"\
            f"type telephone in +YXXXXXXXXXX format to get user by telephone\n"\
            f"type username in @XXXXXXXXXX format to get user by username\n"\
            f"type id in XXXX format to get user by id\n"\
            f"forward message to get origin sender user"
    with open("log.txt","a") as f:
        f.write(message_to_str(message)+'\n')
    await message.answer(strToSend,parse_mode='html')

@dp.message_handler(commands=['get_me'])
async def send(message: types.Message):
    with open("log.txt","a") as f:
        f.write(message_to_str(message)+'\n')
    await message.answer(user_info_from_message(message),parse_mode='html')

@dp.message_handler(commands=['about'])
async def send(message: types.Message):
    with open("log.txt","a") as f:
        f.write(message_to_str(message)+'\n')
    await message.answer("Bot by @teadove, power by pyrogram and aoigram, best python telegram bot wrappers!\nSourse code: https://gitlab.com/TeaDove/get_useridbot\n(i'm not into licencing, so you can do anything with my code)\nIf you wanna help me, send some money to <code>4276380030569342</code>",parse_mode='html')

@dp.message_handler(lambda message: "forward_from" in message and message["forward_from"]["is_bot"]==False)
async def send(message: types.Message):
    strToSend=message_to_str(message)
    await message.answer(f'Start searching for user by <b>forwarded message</b>',parse_mode='html')    
    with Client(session_name="my_account", api_id=config['credentials']['pyrogram_api_id'], api_hash=config['credentials']['pyrogram_api_hash']) as app:
        try:
            user=app.get_users(message["forward_from"]["username"])
        except:
            await message.answer('Something went wrong!',parse_mode='html')
        else:
            strToSend+=' '+user_to_str(user)
            await message.answer(user_info_from_user(user),parse_mode='html')
    with open("log.txt",'a') as f:
        f.write(strToSend+'\n')

@dp.message_handler(regexp='^\d+$')
async def send(message: types.Message):
    strToSend=message_to_str(message)
    await message.answer(f'Start seariching for user by this <b>id</b>: {message.text}',parse_mode='html')    
    with Client(session_name="my_account", api_id=config['credentials']['pyrogram_api_id'], api_hash=config['credentials']['pyrogram_api_hash']) as app:
        try:
            user=app.get_users(message.text[0:13])
        except:
            await message.answer('No user with this id\nor telegram blocks me from this user',parse_mode='html')
        else:
            strToSend+=" "+user_to_str(user)
            await message.answer(user_info_from_user(user),parse_mode='html')
    with open("log.txt",'a') as f:
        f.write(strToSend+'\n')

@dp.message_handler(regexp='^@[A-z][A-z0-9_]{4,31}$')
async def send(message: types.Message):
    strToSend=message_to_str(message)
    await message.answer(f'Start seariching for user by this <b>username</b>: {message.text}',parse_mode='html')    
    with Client(session_name="my_account", api_id=config['credentials']['pyrogram_api_id'], api_hash=config['credentials']['pyrogram_api_hash']) as app:
        try:
            user=app.get_users(message.text[1:34])
        except:
            await message.answer('No user with this username',parse_mode='html')
        else:
            strToSend+=" "+user_to_str(user)
            await message.answer(user_info_from_user(user),parse_mode='html')
    with open("log.txt",'a') as f:
        f.write(strToSend+'\n')


@dp.message_handler(regexp='^\+\d{11,15}$')
async def send(message: types.Message):
    strToSend=message_to_str(message)
    await message.answer(f'Start searching for user by this <b>number</b>: {message.text}',parse_mode='html')    
    with Client(session_name="my_account", api_id=config['credentials']['pyrogram_api_id'], api_hash=config['credentials']['pyrogram_api_hash']) as app:
        try:
            app.add_contacts([InputPhoneContact(message.text[1:17], "FromGet_useridbot")]) 
            user = app.get_users(message.text[1:17])
            userId = user.id
            app.delete_contacts([message.text[1:17]])
            user=app.get_users(userId) #Why this hard? Cause if we will simply try get users info from number, we wont be able get his real first and second names 
        except:
            await message.answer('No user with this phone number or his account cannot be found with phone number',parse_mode='html')
        else:
            strToSend+=" "+user_to_str(user)
            await message.answer(user_info_from_user(user),parse_mode='html')
    with open("log.txt",'a') as f:
        f.write(strToSend+'\n')


@dp.message_handler(lambda message: message["from"]["id"]==int(config['settings']['owner_id']) and re.match(r'-\d{1,1}',message.text))
async def send(message: types.Message):
    command = int(message.text[1])
    with open("log.txt",'r') as f:
        strToSend = f.read()
    if command == 1:
        strToSend="1 - this help\n2 - get last 20 logs from ALL users\n3 - get last 20 logs from all non-owner users\n4 - get !ALL! logs from ALL users\n5 - get !ALL! logs frol all non-owner users"
    elif command == 2:
        strToSend='\n'.join(strToSend.splitlines()[-20:])
    elif command == 3:     
        strToSend='\n'.join(strToSend.splitlines()[-20:])
        strToSend = '\n'.join([line for line in strToSend.splitlines() if len(line.split())>=4 and line.split()[3]!='owner'])
    elif command == 4:
        pass
    elif command == 5:
        strToSend = '\n'.join([line for line in strToSend.splitlines() if len(line.split())>=4 and line.split()[3]!='owner'])
    else:
        strToSend="I don't have command with this index"
    for index in range(0,len(strToSend)//4000+1):
        await message.answer(f'{strToSend[4000*index:4000*(index+1)]}')



@dp.message_handler()
async def send(message: types.Message):
    await message.answer("You have typed something in wrong format:(")


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)
